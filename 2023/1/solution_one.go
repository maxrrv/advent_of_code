package main

import (
	"fmt"
	"log"
	"os"
	"regexp"
	"strconv"
	"strings"
)

var digitAsAlphaRegExp = regexp.MustCompile(`one|two|three|four|five|six|seven|eight|nine|zero`)
var digitAsAlphaRegExpReverse = regexp.MustCompile(`nine|eight|seven|six|five|four|three|two|one`)
var alphaRegExp = regexp.MustCompile(`[a-zA-Z]`)

var tenMB = 10000000


func getSingleDigit(input string) int {
  combined := strings.TrimSpace(string(input[0])) + strings.TrimSpace(string(input[0]))
  fmt.Printf("single digit combined: %s\n", combined)
  singleDigit, err  := strconv.Atoi(combined)
  if err != nil {
    log.Fatal(err)
  }

  return singleDigit
}

func getDoubleDigit(input string) int {
  first := strings.TrimSpace(string(input[0]))
  last := strings.TrimSpace(string(input[len(input)-1]))
  combined := first + last
  fmt.Printf("double digit combined: %s\n", combined)
  doubleDigit, err  := strconv.Atoi(combined)
  if err != nil {
    log.Fatal(err)
  }

  return doubleDigit
}

func normalizeCalibrationString(input string) string {
  fmt.Printf("input before transformation: %s\n", input)
  for {
    value := digitAsAlphaRegExpReverse.FindString(input) 
    // fmt.Printf("value inside loop before replace: %s\n", value)

    if value == "" {
      break
    }

    switch value {
    case "one": input = strings.Replace(input, "one", "1e", 1)
    case "two": input = strings.Replace(input, "two", "2o", 1)
    case "three": input = strings.Replace(input, "three", "3e", 1)
    case "four": input = strings.Replace(input, "four", "4r", 1)
    case "five": input = strings.Replace(input, "five", "5e", 1)
    case "six": input = strings.Replace(input, "six", "6x", 1)
    case "seven": input = strings.Replace(input, "seven", "7n", 1)
    case "eight": input = strings.Replace(input, "eight", "8t", 1)
    case "nine": input = strings.Replace(input, "nine", "9e", 1)
    }
  }

  input = alphaRegExp.ReplaceAllString(input, "")
  fmt.Printf("Input after transformation: %s\n", input)
  return input
}

func getInput () []string {
  file, err := os.Open("input")
  if err != nil {
    log.Fatal(err)
  }

  data := make([]byte, tenMB)
  file.Read(data)

  splitString := strings.Split(strings.TrimSpace(string(data)), "\n")
  splitString = splitString[:len(splitString)-1]

  return splitString
}

func main() {
  splitString := getInput()
  fmt.Printf("Number of lines: %d\n", len(splitString))

  result := 0

  for i := 0; i < len(splitString); i++ {
    tmp := normalizeCalibrationString(splitString[i])

    if len(tmp) == 1 {
      result += getSingleDigit(tmp)
    }

    if len(tmp) > 1 {
      result += getDoubleDigit(tmp)
    }
    fmt.Printf("interim Result: %d\n", result)

  }

  fmt.Printf("Final Result: %d\n", result)
}
